from django.shortcuts import render ,HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm ,AuthenticationForm , PasswordChangeForm ,UserChangeForm
from .forms import RegistratinForm ,profileForm,profileAdminForm
from django.contrib import messages
from django.contrib.auth import authenticate , login , logout ,update_session_auth_hash
from django.contrib.auth.models import User

def sign_up(request):
    if request.method=='POST':
        fm=RegistratinForm(request.POST)
        if fm.is_valid():
            fm.save()
            messages.success(request ,"account created successfully")
    else:
        fm=RegistratinForm()
    return render (request , 'Registration.html', {'form':fm})
def login_sys(request):
    if not request.user.is_authenticated:
        if request.method=='POST':
            fm=AuthenticationForm(request=request , data=request.POST)
            if fm.is_valid():
                uname=fm.cleaned_data['username']
                pwd=fm.cleaned_data['password']
                user=authenticate(username=uname , password=pwd)
                if user is not None:
                    login(request ,user)
                    messages.success(request , "logged in successfully")
                    return HttpResponseRedirect('/profile/')
        else:
            fm=AuthenticationForm()

        return render (request , 'login.html',{'form':fm})
    else:
        return HttpResponseRedirect('/profile/')
def profile_show(request):
    if request.user.is_authenticated :
        if request.method=='POST':
            if request.user.is_superuser==True:
                fm=profileAdminForm(request.POST ,instance=request.user)
                users=User.objects.all()
            else:
                users=None
                fm=profileForm(request.POST , instance=request.user)
            if fm.is_valid():
                messages.success(request , "your profile updated")
                fm.save()
        else:
            if request.user.is_superuser==True:
                fm=profileAdminForm(instance=request.user)
                users=User.objects.all()
            else:
                fm=profileForm(instance=request.user)
                users=None
        return render (request , 'profile.html' ,{'name':request.user , 'form':fm , 'users':users})
    else:
        return HttpResponseRedirect('/login/')
# Create your views here.
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/login/')
def changepwd(request):
    if request.user.is_authenticated :
        if request.method=='POST':
            fm=PasswordChangeForm(user=request.user , data=request.POST)
            if fm.is_valid():
                fm.save()
                update_session_auth_hash(request, fm.user)
                messages.success(request,"password changed successfully")
                return HttpResponseRedirect('/profile/')
        else:
            fm=PasswordChangeForm(user=request.user)
        return render(request , "changepwd.html",{'form':fm})
    else:
        return HttpResponseRedirect('/login/')
def user_detail(request ,id):
    if request.user.is_authenticated:
        pi=User.objects.get(pk=id)
        fm=profileAdminForm(instance=pi)
        return render(request , 'userdetail.html' ,{'form':fm})
    else:
        return HttpResponseRedirect('/login/')
