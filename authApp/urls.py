from django.urls import path ,include
from . import views

urlpatterns = [
    path('', views.sign_up , name='sign_up'),
    path('profile/', views.profile_show, name='profile'),
    path('login/', views.login_sys , name='login'),
    path('logout/', views.user_logout , name='logout'),
    path('changepwd/' , views.changepwd , name='changepwd'),
    path('user_detail/<int:id>',views.user_detail ,name='user_detail'),
]
